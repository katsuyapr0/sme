const express = require('express');
var bodyParser = require("body-parser");
const app = express();
const server = require('http').Server(app);
const port = process.env.PORT || 3000;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));



// collect request info
var requests = [];
var requestTrimThreshold = 5000;
var requestTrimSize = 4000;
app.use(express.static(__dirname + '/public'));
app.use(function (req, res, next) {
    console.log("request");
    requests.push(Date.now());
    // now keep requests array from growing forever
    if (requests.length > requestTrimThreshold) {
        requests = requests.slice(0, requests.length - requestTrimSize);
    }
    next();
});


app.get("/test", function (req, res) {
    var cnt = requests.length;
    res.json({ requestsLastMinute: cnt });
});



server.listen(port, () => console.log('listening on port1 ' + port));
